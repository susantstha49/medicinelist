const countries = [
  {
    name: "Afghanistan",
    code: "AF",
  },
  {
    name: "Åland Islands",
    code: "AX",
  },
  {
    name: "Albania",
    code: "AL",
  },
  {
    name: "Algeria",
    code: "DZ",
  },
  {
    name: "American Samoa",
    code: "AS",
  },
  {
    name: "AndorrA",
    code: "AD",
  },
  {
    name: "Angola",
    code: "AO",
  },
  {
    name: "Anguilla",
    code: "AI",
  },
  {
    name: "Antarctica",
    code: "AQ",
  },
  {
    name: "Antigua and Barbuda",
    code: "AG",
  },
  {
    name: "Argentina",
    code: "AR",
  },
  {
    name: "Armenia",
    code: "AM",
  },
  {
    name: "Aruba",
    code: "AW",
  },
  {
    name: "Australia",
    code: "AU",
  },
  {
    name: "Austria",
    code: "AT",
  },
  {
    name: "Azerbaijan",
    code: "AZ",
  },
  {
    name: "Bahamas",
    code: "BS",
  },
  {
    name: "Bahrain",
    code: "BH",
  },
  {
    name: "Bangladesh",
    code: "BD",
  },
  {
    name: "Barbados",
    code: "BB",
  },
  {
    name: "Belarus",
    code: "BY",
  },
  {
    name: "Belgium",
    code: "BE",
  },
  {
    name: "Belize",
    code: "BZ",
  },
  {
    name: "Benin",
    code: "BJ",
  },
  {
    name: "Bermuda",
    code: "BM",
  },
  {
    name: "Bhutan",
    code: "BT",
  },
  {
    name: "Bolivia",
    code: "BO",
  },
  {
    name: "Bosnia and Herzegovina",
    code: "BA",
  },
  {
    name: "Botswana",
    code: "BW",
  },
  {
    name: "Bouvet Island",
    code: "BV",
  },
  {
    name: "Brazil",
    code: "BR",
  },
  {
    name: "British Indian Ocean Territory",
    code: "IO",
  },
  {
    name: "Brunei Darussalam",
    code: "BN",
  },
  {
    name: "Bulgaria",
    code: "BG",
  },
  {
    name: "Burkina Faso",
    code: "BF",
  },
  {
    name: "Burundi",
    code: "BI",
  },
  {
    name: "Cambodia",
    code: "KH",
  },
  {
    name: "Cameroon",
    code: "CM",
  },
  {
    name: "Canada",
    code: "CA",
  },
  {
    name: "Cape Verde",
    code: "CV",
  },
  {
    name: "Cayman Islands",
    code: "KY",
  },
  {
    name: "Central African Republic",
    code: "CF",
  },
  {
    name: "Chad",
    code: "TD",
  },
  {
    name: "Chile",
    code: "CL",
  },
  {
    name: "China",
    code: "CN",
  },
  {
    name: "Christmas Island",
    code: "CX",
  },
  {
    name: "Cocos (Keeling) Islands",
    code: "CC",
  },
  {
    name: "Colombia",
    code: "CO",
  },
  {
    name: "Comoros",
    code: "KM",
  },
  {
    name: "Congo",
    code: "CG",
  },
  {
    name: "Congo, The Democratic Republic of the",
    code: "CD",
  },
  {
    name: "Cook Islands",
    code: "CK",
  },
  {
    name: "Costa Rica",
    code: "CR",
  },
  {
    name: "Cote D'Ivoire",
    code: "CI",
  },
  {
    name: "Croatia",
    code: "HR",
  },
  {
    name: "Cuba",
    code: "CU",
  },
  {
    name: "Cyprus",
    code: "CY",
  },
  {
    name: "Czech Republic",
    code: "CZ",
  },
  {
    name: "Denmark",
    code: "DK",
  },
  {
    name: "Djibouti",
    code: "DJ",
  },
  {
    name: "Dominica",
    code: "DM",
  },
  {
    name: "Dominican Republic",
    code: "DO",
  },
  {
    name: "Ecuador",
    code: "EC",
  },
  {
    name: "Egypt",
    code: "EG",
  },
  {
    name: "El Salvador",
    code: "SV",
  },
  {
    name: "Equatorial Guinea",
    code: "GQ",
  },
  {
    name: "Eritrea",
    code: "ER",
  },
  {
    name: "Estonia",
    code: "EE",
  },
  {
    name: "Ethiopia",
    code: "ET",
  },
  {
    name: "Falkland Islands (Malvinas)",
    code: "FK",
  },
  {
    name: "Faroe Islands",
    code: "FO",
  },
  {
    name: "Fiji",
    code: "FJ",
  },
  {
    name: "Finland",
    code: "FI",
  },
  {
    name: "France",
    code: "FR",
  },
  {
    name: "French Guiana",
    code: "GF",
  },
  {
    name: "French Polynesia",
    code: "PF",
  },
  {
    name: "French Southern Territories",
    code: "TF",
  },
  {
    name: "Gabon",
    code: "GA",
  },
  {
    name: "Gambia",
    code: "GM",
  },
  {
    name: "Georgia",
    code: "GE",
  },
  {
    name: "Germany",
    code: "DE",
  },
  {
    name: "Ghana",
    code: "GH",
  },
  {
    name: "Gibraltar",
    code: "GI",
  },
  {
    name: "Greece",
    code: "GR",
  },
  {
    name: "Greenland",
    code: "GL",
  },
  {
    name: "Grenada",
    code: "GD",
  },
  {
    name: "Guadeloupe",
    code: "GP",
  },
  {
    name: "Guam",
    code: "GU",
  },
  {
    name: "Guatemala",
    code: "GT",
  },
  {
    name: "Guernsey",
    code: "GG",
  },
  {
    name: "Guinea",
    code: "GN",
  },
  {
    name: "Guinea-Bissau",
    code: "GW",
  },
  {
    name: "Guyana",
    code: "GY",
  },
  {
    name: "Haiti",
    code: "HT",
  },
  {
    name: "Heard Island and Mcdonald Islands",
    code: "HM",
  },
  {
    name: "Holy See (Vatican City State)",
    code: "VA",
  },
  {
    name: "Honduras",
    code: "HN",
  },
  {
    name: "Hong Kong",
    code: "HK",
  },
  {
    name: "Hungary",
    code: "HU",
  },
  {
    name: "Iceland",
    code: "IS",
  },
  {
    name: "India",
    code: "IN",
  },
  {
    name: "Indonesia",
    code: "ID",
  },
  {
    name: "Iran, Islamic Republic Of",
    code: "IR",
  },
  {
    name: "Iraq",
    code: "IQ",
  },
  {
    name: "Ireland",
    code: "IE",
  },
  {
    name: "Isle of Man",
    code: "IM",
  },
  {
    name: "Israel",
    code: "IL",
  },
  {
    name: "Italy",
    code: "IT",
  },
  {
    name: "Jamaica",
    code: "JM",
  },
  {
    name: "Japan",
    code: "JP",
  },
  {
    name: "Jersey",
    code: "JE",
  },
  {
    name: "Jordan",
    code: "JO",
  },
  {
    name: "Kazakhstan",
    code: "KZ",
  },
  {
    name: "Kenya",
    code: "KE",
  },
  {
    name: "Kiribati",
    code: "KI",
  },
  {
    name: "Korea, Democratic People'S Republic of",
    code: "KP",
  },
  {
    name: "Korea, Republic of",
    code: "KR",
  },
  {
    name: "Kuwait",
    code: "KW",
  },
  {
    name: "Kyrgyzstan",
    code: "KG",
  },
  {
    name: "Lao People'S Democratic Republic",
    code: "LA",
  },
  {
    name: "Latvia",
    code: "LV",
  },
  {
    name: "Lebanon",
    code: "LB",
  },
  {
    name: "Lesotho",
    code: "LS",
  },
  {
    name: "Liberia",
    code: "LR",
  },
  {
    name: "Libyan Arab Jamahiriya",
    code: "LY",
  },
  {
    name: "Liechtenstein",
    code: "LI",
  },
  {
    name: "Lithuania",
    code: "LT",
  },
  {
    name: "Luxembourg",
    code: "LU",
  },
  {
    name: "Macao",
    code: "MO",
  },
  {
    name: "Macedonia, The Former Yugoslav Republic of",
    code: "MK",
  },
  {
    name: "Madagascar",
    code: "MG",
  },
  {
    name: "Malawi",
    code: "MW",
  },
  {
    name: "Malaysia",
    code: "MY",
  },
  {
    name: "Maldives",
    code: "MV",
  },
  {
    name: "Mali",
    code: "ML",
  },
  {
    name: "Malta",
    code: "MT",
  },
  {
    name: "Marshall Islands",
    code: "MH",
  },
  {
    name: "Martinique",
    code: "MQ",
  },
  {
    name: "Mauritania",
    code: "MR",
  },
  {
    name: "Mauritius",
    code: "MU",
  },
  {
    name: "Mayotte",
    code: "YT",
  },
  {
    name: "Mexico",
    code: "MX",
  },
  {
    name: "Micronesia, Federated States of",
    code: "FM",
  },
  {
    name: "Moldova, Republic of",
    code: "MD",
  },
  {
    name: "Monaco",
    code: "MC",
  },
  {
    name: "Mongolia",
    code: "MN",
  },
  {
    name: "Montenegro",
    code: "ME",
  },
  {
    name: "Montserrat",
    code: "MS",
  },
  {
    name: "Morocco",
    code: "MA",
  },
  {
    name: "Mozambique",
    code: "MZ",
  },
  {
    name: "Myanmar",
    code: "MM",
  },
  {
    name: "Namibia",
    code: "NA",
  },
  {
    name: "Nauru",
    code: "NR",
  },
  {
    name: "Nepal",
    code: "NP",
  },
  {
    name: "Netherlands",
    code: "NL",
  },
  {
    name: "Netherlands Antilles",
    code: "AN",
  },
  {
    name: "New Caledonia",
    code: "NC",
  },
  {
    name: "New Zealand",
    code: "NZ",
  },
  {
    name: "Nicaragua",
    code: "NI",
  },
  {
    name: "Niger",
    code: "NE",
  },
  {
    name: "Nigeria",
    code: "NG",
  },
  {
    name: "Niue",
    code: "NU",
  },
  {
    name: "Norfolk Island",
    code: "NF",
  },
  {
    name: "Northern Mariana Islands",
    code: "MP",
  },
  {
    name: "Norway",
    code: "NO",
  },
  {
    name: "Oman",
    code: "OM",
  },
  {
    name: "Pakistan",
    code: "PK",
  },
  {
    name: "Palau",
    code: "PW",
  },
  {
    name: "Palestinian Territory, Occupied",
    code: "PS",
  },
  {
    name: "Panama",
    code: "PA",
  },
  {
    name: "Papua New Guinea",
    code: "PG",
  },
  {
    name: "Paraguay",
    code: "PY",
  },
  {
    name: "Peru",
    code: "PE",
  },
  {
    name: "Philippines",
    code: "PH",
  },
  {
    name: "Pitcairn",
    code: "PN",
  },
  {
    name: "Poland",
    code: "PL",
  },
  {
    name: "Portugal",
    code: "PT",
  },
  {
    name: "Puerto Rico",
    code: "PR",
  },
  {
    name: "Qatar",
    code: "QA",
  },
  {
    name: "Reunion",
    code: "RE",
  },
  {
    name: "Romania",
    code: "RO",
  },
  {
    name: "Russian Federation",
    code: "RU",
  },
  {
    name: "RWANDA",
    code: "RW",
  },
  {
    name: "Saint Helena",
    code: "SH",
  },
  {
    name: "Saint Kitts and Nevis",
    code: "KN",
  },
  {
    name: "Saint Lucia",
    code: "LC",
  },
  {
    name: "Saint Pierre and Miquelon",
    code: "PM",
  },
  {
    name: "Saint Vincent and the Grenadines",
    code: "VC",
  },
  {
    name: "Samoa",
    code: "WS",
  },
  {
    name: "San Marino",
    code: "SM",
  },
  {
    name: "Sao Tome and Principe",
    code: "ST",
  },
  {
    name: "Saudi Arabia",
    code: "SA",
  },
  {
    name: "Senegal",
    code: "SN",
  },
  {
    name: "Serbia",
    code: "RS",
  },
  {
    name: "Seychelles",
    code: "SC",
  },
  {
    name: "Sierra Leone",
    code: "SL",
  },
  {
    name: "Singapore",
    code: "SG",
  },
  {
    name: "Slovakia",
    code: "SK",
  },
  {
    name: "Slovenia",
    code: "SI",
  },
  {
    name: "Solomon Islands",
    code: "SB",
  },
  {
    name: "Somalia",
    code: "SO",
  },
  {
    name: "South Africa",
    code: "ZA",
  },
  {
    name: "South Georgia and the South Sandwich Islands",
    code: "GS",
  },
  {
    name: "Spain",
    code: "ES",
  },
  {
    name: "Sri Lanka",
    code: "LK",
  },
  {
    name: "Sudan",
    code: "SD",
  },
  {
    name: "Suriname",
    code: "SR",
  },
  {
    name: "Svalbard and Jan Mayen",
    code: "SJ",
  },
  {
    name: "Swaziland",
    code: "SZ",
  },
  {
    name: "Sweden",
    code: "SE",
  },
  {
    name: "Switzerland",
    code: "CH",
  },
  {
    name: "Syrian Arab Republic",
    code: "SY",
  },
  {
    name: "Taiwan, Province of China",
    code: "TW",
  },
  {
    name: "Tajikistan",
    code: "TJ",
  },
  {
    name: "Tanzania, United Republic of",
    code: "TZ",
  },
  {
    name: "Thailand",
    code: "TH",
  },
  {
    name: "Timor-Leste",
    code: "TL",
  },
  {
    name: "Togo",
    code: "TG",
  },
  {
    name: "Tokelau",
    code: "TK",
  },
  {
    name: "Tonga",
    code: "TO",
  },
  {
    name: "Trinidad and Tobago",
    code: "TT",
  },
  {
    name: "Tunisia",
    code: "TN",
  },
  {
    name: "Turkey",
    code: "TR",
  },
  {
    name: "Turkmenistan",
    code: "TM",
  },
  {
    name: "Turks and Caicos Islands",
    code: "TC",
  },
  {
    name: "Tuvalu",
    code: "TV",
  },
  {
    name: "Uganda",
    code: "UG",
  },
  {
    name: "Ukraine",
    code: "UA",
  },
  {
    name: "United Arab Emirates",
    code: "AE",
  },
  {
    name: "United Kingdom",
    code: "GB",
  },
  {
    name: "United States",
    code: "US",
  },
  {
    name: "United States Minor Outlying Islands",
    code: "UM",
  },
  {
    name: "Uruguay",
    code: "UY",
  },
  {
    name: "Uzbekistan",
    code: "UZ",
  },
  {
    name: "Vanuatu",
    code: "VU",
  },
  {
    name: "Venezuela",
    code: "VE",
  },
  {
    name: "Viet Nam",
    code: "VN",
  },
  {
    name: "Virgin Islands, British",
    code: "VG",
  },
  {
    name: "Virgin Islands, U.S.",
    code: "VI",
  },
  {
    name: "Wallis and Futuna",
    code: "WF",
  },
  {
    name: "Western Sahara",
    code: "EH",
  },
  {
    name: "Yemen",
    code: "YE",
  },
  {
    name: "Zambia",
    code: "ZM",
  },
  {
    name: "Zimbabwe",
    code: "ZW",
  },
];

let drugs = [
  {
    Generic: "Zidovudine",
    "Dosage form & strength": "Solution, Oral 50mg/5mL",
    Form: "Oral Solution  ",
    Strength: "50mg/5mL",
    Package_1: "100ml",
    Package_2: "240ml",
    Pack_1: "Bottle",
    Pack_2: "Bottle",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Lamivudine/Zidovudine",
    "Dosage form & strength": "Tablet, Film-coated 150mg/300mg",
    Form: "Film-coated Tablet ",
    Strength: "150mg/300mg",
    Package_1: "60",
    Package_2: "10",
    Pack_1: "Botle",
    Pack_2: "Blister",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Ritonavir",
    "Dosage form & strength": "Solution, Oral 80mg/mL",
    Form: "Oral Solution  80mg/mL",
    Strength: "80mg/mL",
    Package_1: "90ml",
    Package_2: "",
    Pack_1: "Bottle",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Lopinavir/Ritonavir",
    "Dosage form & strength": "Solution, Oral 80mg/mL/20mg/mL",
    Form: "Oral Solution ",
    Strength: " 80mg/mL/20mg/mL",
    Package_1: "60ml",
    Package_2: "",
    Pack_1: "Bottle",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Abacavir (sulfate)",
    "Dosage form & strength": "Tablet, Film-coated 300mg",
    Form: "Film-coated Tablet ",
    Strength: "300mg",
    Package_1: "60",
    Package_2: "",
    Pack_1: "Blister",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Abacavir (sulfate)",
    "Dosage form & strength": "Solution, Oral 20mg/mL",
    Form: "Oral Solution  ",
    Strength: "20mg/mL",
    Package_1: " 240mL",
    Package_2: "",
    Pack_1: "Bottle",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Zidovudine",
    "Dosage form & strength": "Capsules, hard 100mg",
    Form: "Hard Capsules ",
    Strength: "100mg",
    Package_1: "100",
    Package_2: "",
    Pack_1: "Blister",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Zidovudine",
    "Dosage form & strength": "Capsules, hard 250mg",
    Form: "Hard Capsules ",
    Strength: "250mg",
    Package_1: "40",
    Package_2: "",
    Pack_1: "Blister",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Lamivudine/Zidovudine",
    "Dosage form & strength": "Tablet, Film-coated 150mg/300mg",
    Form: "Film-coated Tablet ",
    Strength: "150mg/300mg",
    Package_1: "60",
    Package_2: "60",
    Pack_1: "Bottle",
    Pack_2: "Blister",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Zidovudine",
    "Dosage form & strength": "Solution for injection 10mg/mL",
    Form: "Solution for Injection ",
    Strength: "10mg/mL",
    Package_1: "20ml",
    Package_2: "",
    Pack_1: "Vial",
    Pack_2: "",
    Quanty_1: "5",
    Quantity_2: "",
  },
  {
    Generic: "Zidovudine",
    "Dosage form & strength": "Solution, Oral 50mg/5mL",
    Form: "Oral Solution  ",
    Strength: "50mg/5mL",
    Package_1: "200ml",
    Package_2: "",
    Pack_1: "Bottle",
    Pack_2: "",
    Quanty_1: "1",
    Quantity_2: "",
  },
  {
    Generic: "Lamivudine",
    "Dosage form & strength": "Tablet, Film-coated 150mg",
    Form: "Film-coated Tablet ",
    Strength: "150mg",
    Package_1: "60",
    Package_2: "",
    Pack_1: "Bottle",
    Pack_2: "",
    Quanty_1: "1",
    Quantity_2: "",
  },
  {
    Generic: "Lamivudine",
    "Dosage form & strength": "Solution, Oral 10mg/mL",
    Form: "Oral Solution  ",
    Strength: "10mg/mL",
    Package_1: "240ml",
    Package_2: "",
    Pack_1: "Bottle",
    Pack_2: "",
    Quanty_1: "1",
    Quantity_2: "",
  },
  {
    Generic: "Nevirapine",
    "Dosage form & strength": "Suspension, Oral 50mg/5mL",
    Form: "Oral Suspension",
    Strength: "50mg/5mL",
    Package_1: "100ml",
    Package_2: "240ml",
    Pack_1: "Bottle",
    Pack_2: "Bottle",
    Quanty_1: "1",
    Quantity_2: "1",
  },
  {
    Generic: "Nevirapine",
    "Dosage form & strength": "Tablet 200mg",
    Form: "Tablet ",
    Strength: "200mg",
    Package_1: "60",
    Package_2: "",
    Pack_1: "Bottle",
    Pack_2: "",
    Quanty_1: "1",
    Quantity_2: "",
  },
  {
    Generic: "Lamivudine/Zidovudine",
    "Dosage form & strength": "Tablet, Film-coated 150mg/300mg",
    Form: "Film-coated Tablet ",
    Strength: "150mg/300mg",
    Package_1: "60",
    Package_2: "10",
    Pack_1: "Bottle",
    Pack_2: "Blister",
    Quanty_1: "1",
    Quantity_2: "6",
  },
  {
    Generic: "Lamivudine/Zidovudine",
    "Dosage form & strength": "Tablet, Film-coated 150mg/300mg",
    Form: "Film-coated Tablet ",
    Strength: "150mg/300mg",
    Package_1: "60",
    Package_2: "",
    Pack_1: "HDPE Container",
    Pack_2: "",
    Quanty_1: "1",
    Quantity_2: "",
  },
  {
    Generic: "Efavirenz",
    "Dosage form & strength": "Tablet, Film-coated 600mg",
    Form: "Film-coated Tablet",
    Strength: " 600mg",
    Package_1: "30",
    Package_2: "10",
    Pack_1: "Bottle",
    Pack_2: "Blister",
    Quanty_1: "1",
    Quantity_2: "3",
  },
  {
    Generic: "Efavirenz",
    "Dosage form & strength": "Tablet, Film-coated 600mg",
    Form: "Film-coated Tablet ",
    Strength: "600mg",
    Package_1: "30",
    Package_2: "",
    Pack_1: "Bottle",
    Pack_2: "",
    Quanty_1: "1",
    Quantity_2: "",
  },
  {
    Generic: "Efavirenz",
    "Dosage form & strength": "Solution, Oral 30mg",
    Form: "Oral Solution  ",
    Strength: "30mg",
    Package_1: "180ml",
    Package_2: "",
    Pack_1: "Bottle",
    Pack_2: "",
    Quanty_1: "1",
    Quantity_2: "",
  },
  {
    Generic: "Efavirenz",
    "Dosage form & strength": "Tablet, Film-coated 600mg",
    Form: "Film-coated Tablet ",
    Strength: "600mg",
    Package_1: "30",
    Package_2: "",
    Pack_1: "Bottle",
    Pack_2: "",
    Quanty_1: "1",
    Quantity_2: "",
  },
  {
    Generic: "Lamivudine",
    "Dosage form & strength": "Tablet, Film-coated 150mg",
    Form: "Film-coated Tablet ",
    Strength: "150mg",
    Package_1: "30",
    Package_2: "60",
    Pack_1: "Bottle",
    Pack_2: "Bottle",
    Quanty_1: "1",
    Quantity_2: "1",
  },
  {
    Generic: "Lamivudine",
    "Dosage form & strength": "Tablet, Film-coated 300mg",
    Form: "Film-coated Tablet ",
    Strength: "300mg",
    Package_1: "30",
    Package_2: "60",
    Pack_1: "Bottle",
    Pack_2: "Bottle",
    Quanty_1: "1",
    Quantity_2: "1",
  },
  {
    Generic: "Abacavir (sulfate)",
    "Dosage form & strength": "Tablet 300mg",
    Form: "Tablet",
    Strength: " 300mg",
    Package_1: "60",
    Package_2: "",
    Pack_1: "Bottle",
    Pack_2: "",
    Quanty_1: "1",
    Quantity_2: "",
  },
  {
    Generic: "Lopinavir/Ritonavir",
    "Dosage form & strength": "Tablet, Film-coated 200mg/50mg",
    Form: "Film-coated Tablet ",
    Strength: "200mg/50mg",
    Package_1: "120",
    Package_2: "120",
    Pack_1: "Bottle",
    Pack_2: "Bottle",
    Quanty_1: "1",
    Quantity_2: "3",
  },
  {
    Generic: "Efavirenz",
    "Dosage form & strength": "Tablet, Film-coated 200mg",
    Form: "Film-coated Tablet ",
    Strength: "200mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Efavirenz",
    "Dosage form & strength": "Tablet, coated 600mg",
    Form: "Coated Tablet",
    Strength: "600mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Lamivudine/Zidovudine",
    "Dosage form & strength": "Tablet, Film-coated 150mg/300mg",
    Form: "Film-coated Tablet ",
    Strength: "150mg/300mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Tenofovir disoproxil fumarate",
    "Dosage form & strength": "Tablet, Film-coated 300mg",
    Form: "Film-coated Tablet ",
    Strength: "300mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Efavirenz",
    "Dosage form & strength": "Tablet, Film-coated 600mg",
    Form: "Film-coated Tablet ",
    Strength: "600mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Tenofovir disoproxil fumarate",
    "Dosage form & strength": "Tablet, Film-coated 300mg",
    Form: "Film-coated Tablet ",
    Strength: "300mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Lamivudine/Tenofovir disoproxil fumarate",
    "Dosage form & strength": "Tablet, Film-coated 300mg/300mg",
    Form: "Film-coated Tablet ",
    Strength: "300mg/300mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Emtricitabine/Tenofovir disoproxil fumarate",
    "Dosage form & strength": "Tablet, Film-coated 200mg/300mg",
    Form: "Film-coated Tablet ",
    Strength: "200mg/300mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Atazanavir (sulfate)",
    "Dosage form & strength": "Capsules, hard 150mg",
    Form: "Hard Capsules ",
    Strength: "150mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Lamivudine",
    "Dosage form & strength": "Tablet, Film-coated 150mg",
    Form: "Film-coated Tablet ",
    Strength: "150mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Lamivudine/Nevirapine/Zidovudine",
    "Dosage form & strength": "Tablet, Film-coated 150mg/200mg/300mg",
    Form: "Film-coated Tablet ",
    Strength: "150mg/200mg/300mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Efavirenz",
    "Dosage form & strength": "Tablet, Film-coated 50mg",
    Form: "Film-coated Tablet ",
    Strength: "50mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Efavirenz",
    "Dosage form & strength": "Tablet, Film-coated 200mg",
    Form: "Film-coated Tablet ",
    Strength: "200mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Lamivudine/Nevirapine/Zidovudine",
    "Dosage form & strength": "Tablet, Dispersible 30mg/50mg/60mg",
    Form: "Tablet, Dispersible ",
    Strength: "30mg/50mg/60mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Emtricitabine/Tenofovir disoproxil fumarate",
    "Dosage form & strength": "Tablet, Film-coated 200mg/300mg",
    Form: "Film-coated Tablet ",
    Strength: "200mg/300mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Efavirenz/Emtricitabine/Tenofovir disoproxil fumarate",
    "Dosage form & strength": "Tablet, Film-coated 600mg/200mg/300mg",
    Form: "Film-coated Tablet ",
    Strength: "600mg/200mg/300mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Lamivudine/Tenofovir disoproxil fumarate",
    "Dosage form & strength": "Tablet 300mg/300mg",
    Form: "Tablet ",
    Strength: "300mg/300mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Efavirenz/Lamivudine/Tenofovir disoproxil fumarate",
    "Dosage form & strength": "Tablet, Film-coated 600mg/300mg/300mg",
    Form: "Film-coated Tablet ",
    Strength: "600mg/300mg/300mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Ritonavir",
    "Dosage form & strength": "Tablet, Film-coated 100mg",
    Form: "Film-coated Tablet ",
    Strength: "100mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Zidovudine",
    "Dosage form & strength": "Tablet, Film-coated 300mg",
    Form: "Film-coated Tablet ",
    Strength: "300mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Lamivudine/Zidovudine",
    "Dosage form & strength": "Tablet, Film-coated 150mg/300mg",
    Form: "Film-coated Tablet ",
    Strength: "150mg/300mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Zidovudine",
    "Dosage form & strength": "Solution, Oral 10mg",
    Form: "Oral Solution  ",
    Strength: "10mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Abacavir (sulfate)",
    "Dosage form & strength": "Tablet, Dispersible 60mg",
    Form: "Dispersible Tablet",
    Strength: " 60mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Lamivudine/Zidovudine",
    "Dosage form & strength": "Tablet, Film-coated 150mg/300mg",
    Form: "Film-coated Tablet ",
    Strength: "150mg/300mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Ritonavir",
    "Dosage form & strength": "Tablet, Film-coated 100mg",
    Form: "Film-coated Tablet ",
    Strength: "100mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Lopinavir/Ritonavir",
    "Dosage form & strength": "Tablet, Film-coated 200mg/50mg",
    Form: "Film-coated Tablet",
    Strength: " 200mg/50mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Abacavir (sulfate)",
    "Dosage form & strength": "Tablet, Film-coated 300mg",
    Form: "Film-coated Tablet ",
    Strength: "300mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Emtricitabine/Tenofovir disoproxil fumarate",
    "Dosage form & strength": "Tablet, Film-coated 200mg/300mg",
    Form: "Film-coated Tablet ",
    Strength: "200mg/300mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Efavirenz/Emtricitabine/Tenofovir disoproxil fumarate",
    "Dosage form & strength": "Tablet, Film-coated 600mg/200mg/300mg",
    Form: "Film-coated Tablet ",
    Strength: "600mg/200mg/300mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Efavirenz",
    "Dosage form & strength": "Tablet, Film-coated 600mg",
    Form: "Film-coated Tablet ",
    Strength: "600mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Nevirapine",
    "Dosage form & strength": "Tablet, Dispersible 50mg",
    Form: "Tablet, Dispersible ",
    Strength: "50mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Lamivudine/Nevirapine/Zidovudine",
    "Dosage form & strength": "Tablet, Film-coated 150mg/200mg/300mg",
    Form: "Film-coated Tablet ",
    Strength: "150mg/200mg/300mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Lamivudine/Tenofovir disoproxil fumarate",
    "Dosage form & strength": "Tablet, Film-coated 300mg/300mg",
    Form: "Film-coated Tablet ",
    Strength: "300mg/300mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Tenofovir disoproxil fumarate",
    "Dosage form & strength": "Tablet, Film-coated 300mg",
    Form: "Film-coated Tablet ",
    Strength: "300mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Abacavir (sulfate)/Lamivudine",
    "Dosage form & strength": "Tablet, Dispersible 60mg/30mg",
    Form: "Tablet, Dispersible",
    Strength: " 60mg/30mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Lamivudine/Zidovudine",
    "Dosage form & strength": "Tablet, Film-coated 150mg/300mg",
    Form: "Film-coated Tablet ",
    Strength: "150mg/300mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Lamivudine",
    "Dosage form & strength": "Solution, Oral 10mg/mL",
    Form: "Oral Solution  ",
    Strength: "10mg/mL",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Lamivudine/Nevirapine/Zidovudine",
    "Dosage form & strength": "Tablet, Film-coated 150mg/200mg/300mg",
    Form: "Film-coated Tablet ",
    Strength: "150mg/200mg/300mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Lamivudine/Tenofovir disoproxil fumarate",
    "Dosage form & strength": "Tablet, Film-coated 300mg/300mg",
    Form: "Film-coated Tablet",
    Strength: " 300mg/300mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Zidovudine",
    "Dosage form & strength": "Solution, Oral 10mg/mL",
    Form: "Oral Solution  ",
    Strength: "10mg/mL",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Darunavir (ethanolate)",
    "Dosage form & strength": "Tablet, Film-coated 75mg",
    Form: "Film-coated Tablet ",
    Strength: "75mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Darunavir (ethanolate)",
    "Dosage form & strength": "Tablet, Film-coated 150mg",
    Form: "Film-coated Tablet ",
    Strength: "150mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Darunavir (ethanolate)",
    "Dosage form & strength": "Tablet, Film-coated 600mg",
    Form: "Film-coated Tablet ",
    Strength: "600mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Etravirine",
    "Dosage form & strength": "Tablet, Film-coated 100mg",
    Form: "Film-coated Tablet ",
    Strength: "100mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Tenofovir disoproxil fumarate",
    "Dosage form & strength": "Tablet, Film-coated 300mg",
    Form: "Film-coated Tablet ",
    Strength: "300mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Lamivudine",
    "Dosage form & strength": "Tablet, Film-coated 30mg",
    Form: "Film-coated Tablet ",
    Strength: "30mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Zidovudine",
    "Dosage form & strength": "Tablet, Film-coated 60mg",
    Form: "Film-coated Tablet ",
    Strength: "60mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Efavirenz/Emtricitabine/Tenofovir disoproxil fumarate",
    "Dosage form & strength": "Tablet, Film-coated 600mg/200mg/300mg",
    Form: "Film-coated Tablet",
    Strength: " 600mg/200mg/300mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Efavirenz/Lamivudine/Tenofovir disoproxil fumarate",
    "Dosage form & strength": "Tablet, Film-coated 600mg/300mg/300mg",
    Form: "Film-coated Tablet ",
    Strength: "600mg/300mg/300mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Emtricitabine/Tenofovir disoproxil fumarate",
    "Dosage form & strength": "Tablet, Film-coated 200mg/300mg",
    Form: "Film-coated Tablet ",
    Strength: "200mg/300mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Emtricitabine/Tenofovir disoproxil fumarate",
    "Dosage form & strength": "Tablet, Film-coated 200mg/300mg",
    Form: "Film-coated Tablet ",
    Strength: "200mg/300mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Efavirenz/Emtricitabine/Tenofovir disoproxil fumarate",
    "Dosage form & strength": "Tablet, Film-coated 600mg/200mg/300mg",
    Form: "Film-coated Tablet ",
    Strength: "600mg/200mg/300mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Lamivudine/Zidovudine",
    "Dosage form & strength": "Tablet, Film-coated 30mg/60mg",
    Form: "Film-coated Tablet ",
    Strength: "30mg/60mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Lamivudine/Nevirapine/Zidovudine",
    "Dosage form & strength": "Tablet, Dispersible 30mg/50mg/60mg",
    Form: "Tablet, Dispersible ",
    Strength: "30mg/50mg/60mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Emtricitabine/Tenofovir disoproxil fumarate",
    "Dosage form & strength": "Tablet, Film-coated 200mg/300mg",
    Form: "Film-coated Tablet ",
    Strength: "200mg/300mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Efavirenz/Emtricitabine/Tenofovir disoproxil fumarate",
    "Dosage form & strength": "Tablet, Film-coated 600mg/200mg/300mg",
    Form: "Film-coated Tablet",
    Strength: " 600mg/200mg/300mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Nevirapine",
    "Dosage form & strength": "Tablet 20mg",
    Form: "Tablet ",
    Strength: "20mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Nevirapine",
    "Dosage form & strength": "Tablet 50mg",
    Form: "Tablet ",
    Strength: "50mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Nevirapine",
    "Dosage form & strength": "Tablet 100mg",
    Form: "Tablet ",
    Strength: "100mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Nevirapine",
    "Dosage form & strength": "Tablet 200mg",
    Form: "Tablet ",
    Strength: "200mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Lamivudine/Zidovudine",
    "Dosage form & strength": "Tablet, Dispersible 30mg/60mg",
    Form: "Tablet, Dispersible ",
    Strength: "30mg/60mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Lopinavir/Ritonavir",
    "Dosage form & strength": "Tablet, Film-coated 100mg/25mg",
    Form: "Film-coated Tablet ",
    Strength: "100mg/25mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Lopinavir/Ritonavir",
    "Dosage form & strength": "Tablet, Film-coated 200mg/50mg",
    Form: "Film-coated Tablet ",
    Strength: "200mg/50mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Efavirenz/Lamivudine/Tenofovir disoproxil fumarate",
    "Dosage form & strength": "Tablet, Film-coated 600mg/300mg/300mg",
    Form: "Film-coated Tablet ",
    Strength: "600mg/300mg/300mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Sulfamethoxazole/Trimethoprim",
    "Dosage form & strength": "Tablet 400mg/80mg",
    Form: "Tablet ",
    Strength: "400mg/80mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Sulfamethoxazole/Trimethoprim",
    "Dosage form & strength": "Tablet 800mg/160mg",
    Form: "Tablet ",
    Strength: "800mg/160mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Etravirine",
    "Dosage form & strength": "Tablet 25mg",
    Form: "Tablet",
    Strength: " 25mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Aciclovir",
    "Dosage form & strength": "Powder for solution for injection 250mg/vial",
    Form: "Powder for solution for injection ",
    Strength: "250mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Efavirenz/Lamivudine/Tenofovir disoproxil fumarate",
    "Dosage form & strength": "Tablet, Film-coated 600mg/300mg/300mg",
    Form: "Film-coated Tablet ",
    Strength: "600mg/300mg/300mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Lamivudine/Tenofovir disoproxil fumarate",
    "Dosage form & strength": "Tablet, Film-coated 300mg/300mg",
    Form: "Film-coated Tablet",
    Strength: " 300mg/300mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Ritonavir",
    "Dosage form & strength": "Tablet, Film-coated 25mg",
    Form: "Film-coated Tablet ",
    Strength: "25mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Darunavir (ethanolate)",
    "Dosage form & strength": "Tablet, Film-coated 400mg",
    Form: "Film-coated Tablet ",
    Strength: "400mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Darunavir (ethanolate)",
    "Dosage form & strength": "Tablet, Film-coated 600mg",
    Form: "Film-coated Tablet ",
    Strength: "600mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Lamivudine/Nevirapine/Zidovudine",
    "Dosage form & strength": "Tablet, Film-coated 150mg/200mg/300mg",
    Form: "Film-coated Tablet ",
    Strength: "150mg/200mg/300mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Emtricitabine/Tenofovir disoproxil fumarate",
    "Dosage form & strength": "Tablet, Film-coated 200mg/300mg",
    Form: "Film-coated Tablet ",
    Strength: "200mg/300mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Atazanavir (sulfate)/Ritonavir",
    "Dosage form & strength": "Tablet, Film-coated 300mg/100mg",
    Form: "Film-coated Tablet ",
    Strength: "300mg/100mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Efavirenz",
    "Dosage form & strength": "Tablet, Film-coated 600mg",
    Form: "Film-coated Tablet ",
    Strength: "600mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Dolutegravir (Sodium)",
    "Dosage form & strength": "Tablet, Film-coated 50mg",
    Form: "Film-coated Tablet ",
    Strength: "50mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Abacavir (sulfate)/Lamivudine",
    "Dosage form & strength": "Tablet, Film-coated 600mg/300mg",
    Form: "Film-coated Tablet ",
    Strength: "600mg/300mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Lamivudine",
    "Dosage form & strength": "Tablet, Film-coated 150mg",
    Form: "Film-coated Tablet ",
    Strength: "150mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Lopinavir/Ritonavir",
    "Dosage form & strength": "Tablet, Film-coated 100mg/25mg",
    Form: "Film-coated Tablet ",
    Strength: "100mg/25mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Lamivudine/Zidovudine",
    "Dosage form & strength": "Tablet, Film-coated 150mg/300mg",
    Form: "Film-coated Tablet ",
    Strength: "150mg/300mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Lamivudine/Zidovudine",
    "Dosage form & strength": "Tablet, Film-coated 150mg/300mg",
    Form: "Film-coated Tablet ",
    Strength: "150mg/300mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Abacavir (sulfate)/Lamivudine",
    "Dosage form & strength": "Tablet, Film-coated 600mg/300mg",
    Form: "Film-coated Tablet",
    Strength: " 600mg/300mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Efavirenz",
    "Dosage form & strength": "Tablet, Film-coated 600mg",
    Form: "Film-coated Tablet",
    Strength: " 600mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Lamivudine/Zidovudine",
    "Dosage form & strength": "Tablet, Film-coated 150mg/300mg",
    Form: "Film-coated Tablet ",
    Strength: "150mg/300mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Abacavir (sulfate)/Lamivudine",
    "Dosage form & strength": "Tablet, Dispersible 120mg/60mg",
    Form: "Tablet, Dispersible ",
    Strength: "120mg/60mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Lamivudine/Tenofovir disoproxil fumarate",
    "Dosage form & strength": "Tablet, Film-coated 300mg/300mg",
    Form: "Film-coated Tablet ",
    Strength: "300mg/300mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Lamivudine",
    "Dosage form & strength": "Tablet, Film-coated 300mg",
    Form: "Film-coated Tablet",
    Strength: " 300mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Abacavir (sulfate)",
    "Dosage form & strength": "Tablet, Dispersible 60mg",
    Form: "Tablet, Dispersible",
    Strength: " 60mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Dolutegravir (Sodium)",
    "Dosage form & strength": "Tablet, Film-coated 50mg",
    Form: "Film-coated Tablet ",
    Strength: "50mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Tenofovir disoproxil fumarate",
    "Dosage form & strength": "Tablet, Film-coated 300mg",
    Form: "Film-coated Tablet ",
    Strength: "300mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Dolutegravir (Sodium)",
    "Dosage form & strength": "Tablet, Film-coated 50mg",
    Form: "Film-coated Tablet ",
    Strength: "50mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Efavirenz",
    "Dosage form & strength": "Tablet, Film-coated 600mg",
    Form: "Film-coated Tablet ",
    Strength: "600mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Dolutegravir (Sodium)",
    "Dosage form & strength": "Tablet, Film-coated 50mg",
    Form: "Film-coated Tablet",
    Strength: " 50mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Darunavir (ethanolate)",
    "Dosage form & strength": "Tablet, Film-coated 800mg",
    Form: "Film-coated Tablet ",
    Strength: "800mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Lamivudine/Nevirapine/Zidovudine",
    "Dosage form & strength": "Tablet, Film-coated 150mg/200mg/300mg",
    Form: "Film-coated Tablet ",
    Strength: "150mg/200mg/300mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Darunavir (ethanolate)",
    "Dosage form & strength": "Tablet, Film-coated 600mg",
    Form: "Film-coated Tablet ",
    Strength: "600mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Dolutegravir (Sodium)/Lamivudine/Tenofovir disoproxil fumarate",
    "Dosage form & strength": "Tablet, Film-coated 50mg/300mg/300mg",
    Form: "Film-coated Tablet ",
    Strength: "50mg/300mg/300mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Dolutegravir (Sodium)",
    "Dosage form & strength": "Tablet, Film-coated 50mg",
    Form: "Film-coated Tablet ",
    Strength: "50mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Dolutegravir (Sodium)/Lamivudine/Tenofovir disoproxil fumarate",
    "Dosage form & strength": "Tablet, Film-coated 50mg/300mg/300mg",
    Form: "Film-coated Tablet ",
    Strength: "50mg/300mg/300mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Lopinavir/Ritonavir",
    "Dosage form & strength": "Granules for Oral suspension 40mg/10mg",
    Form: "Granules for Oral suspension ",
    Strength: "40mg/10mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Abacavir (sulfate)/Lamivudine",
    "Dosage form & strength": "Tablet, Film-coated 600mg/300mg",
    Form: "Film-coated Tablet ",
    Strength: "600mg/300mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Dolutegravir (Sodium)/Lamivudine/Tenofovir disoproxil fumarate",
    "Dosage form & strength": "Tablet, Film-coated 50mg/300mg/300mg",
    Form: "Film-coated Tablet ",
    Strength: "50mg/300mg/300mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Efavirenz",
    "Dosage form & strength": "Tablet, Film-coated 600mg",
    Form: "Film-coated Tablet",
    Strength: " 600mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Dolutegravir (Sodium)",
    "Dosage form & strength": "Tablet, Film-coated 50mg",
    Form: "Film-coated Tablet ",
    Strength: "50mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Dolutegravir (Sodium)/Lamivudine/Tenofovir disoproxil fumarate",
    "Dosage form & strength": "Tablet, Film-coated 50mg/300mg/300mg",
    Form: "Film-coated Tablet ",
    Strength: "50mg/300mg/300mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Lamivudine/Tenofovir disoproxil fumarate",
    "Dosage form & strength": "Tablet, Film-coated 300mg/300mg",
    Form: "Film-coated Tablet ",
    Strength: "300mg/300mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Abacavir (sulfate)/Lamivudine",
    "Dosage form & strength": "Tablet, Film-coated 600mg/300mg",
    Form: "Film-coated Tablet",
    Strength: " 600mg/300mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Dolutegravir (Sodium)/Lamivudine/Tenofovir disoproxil fumarate",
    "Dosage form & strength": "Tablet, Film-coated 50mg/300mg/300mg",
    Form: "Film-coated Tablet ",
    Strength: "50mg/300mg/300mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Dolutegravir (Sodium)",
    "Dosage form & strength": "Tablet, Film-coated 50mg",
    Form: "Film-coated Tablet",
    Strength: "50mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Darunavir",
    "Dosage form & strength": "Tablet, Film-coated 400mg",
    Form: "Film-coated Tablet ",
    Strength: "400mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Darunavir",
    "Dosage form & strength": "Tablet, Film-coated 600mg",
    Form: "Film-coated Tablet ",
    Strength: "600mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Darunavir",
    "Dosage form & strength": "Tablet, Film-coated 800mg",
    Form: "Film-coated Tablet ",
    Strength: "800mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Lamivudine/Tenofovir disoproxil fumarate",
    "Dosage form & strength": "Tablet, Film-coated 300mg/300mg",
    Form: "Film-coated Tablet ",
    Strength: "300mg/300mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Dolutegravir (Sodium)/Lamivudine/Tenofovir disoproxil fumarate",
    "Dosage form & strength": "Tablet, Film-coated 50mg/300mg/300mg",
    Form: "Film-coated Tablet ",
    Strength: "50mg/300mg/300mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Efavirenz/Lamivudine/Tenofovir disoproxil fumarate",
    "Dosage form & strength": "Tablet, Film-coated 400mg/300mg/300mg",
    Form: "Film-coated Tablet ",
    Strength: "400mg/300mg/300mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Emtricitabine/Tenofovir disoproxil fumarate",
    "Dosage form & strength": "Tablet, Film-coated 200mg/300mg",
    Form: "Film-coated Tablet ",
    Strength: "200mg/300mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Emtricitabine/Tenofovir disoproxil fumarate",
    "Dosage form & strength": "Tablet, Film-coated 200mg/300mg",
    Form: "Film-coated Tablet ",
    Strength: "200mg/300mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Dolutegravir (Sodium)",
    "Dosage form & strength": "Tablet, Film-coated 50mg",
    Form: "Film-coated Tablet ",
    Strength: "50mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Efavirenz/Lamivudine/Tenofovir disoproxil fumarate",
    "Dosage form & strength": "Tablet, Film-coated 400mg/300mg/300mg",
    Form: "Film-coated Tablet ",
    Strength: "400mg/300mg/300mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Dolutegravir (Sodium)/Lamivudine/Tenofovir disoproxil fumarate",
    "Dosage form & strength": "Tablet, Film-coated 50mg/300mg/300mg",
    Form: "Film-coated Tablet ",
    Strength: "50mg/300mg/300mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Emtricitabine/Tenofovir disoproxil fumarate",
    "Dosage form & strength": "Tablet, Film-coated 200mg/300mg",
    Form: "Film-coated Tablet ",
    Strength: "200mg/300mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Efavirenz/Lamivudine/Tenofovir disoproxil fumarate",
    "Dosage form & strength": "Tablet, Film-coated 600mg/300mg/300mg",
    Form: "Film-coated Tablet ",
    Strength: "600mg/300mg/300mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Dolutegravir (Sodium)/Lamivudine/Tenofovir disoproxil fumarate",
    "Dosage form & strength": "Tablet, Film-coated 50mg/300mg/300mg",
    Form: "Film-coated Tablet ",
    Strength: "50mg/300mg/300mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Efavirenz/Lamivudine/Tenofovir disoproxil fumarate",
    "Dosage form & strength": "Tablet, Film-coated 400mg/300mg/300mg",
    Form: "Film-coated Tablet ",
    Strength: "400mg/300mg/300mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Darunavir",
    "Dosage form & strength": "Tablet, Film-coated 600mg",
    Form: "Film-coated Tablet",
    Strength: " 600mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Atazanavir (sulfate)",
    "Dosage form & strength": "Capsules, hard 300mg",
    Form: "Hard Capsules",
    Strength: " 300mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
  {
    Generic: "Dolutegravir (Sodium)",
    "Dosage form & strength": "Tablet, Film-coated 50mg",
    Form: "Film-coated Tablet ",
    Strength: "50mg",
    Package_1: "",
    Package_2: "",
    Pack_1: "",
    Pack_2: "",
    Quanty_1: "",
    Quantity_2: "",
  },
];

let no = 0;
// drugs.forEach((element) => {
//   no = no + 1;

//   $(" .medicine-lists").append(
//     `
//        <div class="col-md-12">
//         <div class="product-card" id="productCard">
//           <div class="product-details ">
//             <ul class="details-lists d-flex mb-0 justify-content-between list-unstyled">

//               <li class="details-list">
//                 <span class="generic_name">${element.Generic}</span>
//               </li>
//               <li class="details-list">
//                 <span class="form">${element.Form}</span>
//               </li>
//               <li class="details-list">
//                 <span class="strength">${element.Strength}</span>
//               </li>
//               <li>
//                 <span><a href="javascript:void(0)" class="addmed" >+</a></span>
//               </li>
//             </ul>
//           </div>
//         </div>
//       </div>
//       `
//   );
// });

var itemsPerPage = 10;

function renderer(data) {
  $(".medicine-list").empty();
  $.each(data, function (i, v) {
    $(" .medicine-list").append(
      `
       <div class="col-md-12">
        <div class="product-card" id="productCard">
          <div class="product-details ">
            <ul class="details-lists d-flex mb-0 justify-content-between list-unstyled">
            
              <li class="details-list">
                <span class="generic_name">${v.Generic}</span>
              </li>
              <li class="details-list">
                <span class="form">${v.Form}</span>
              </li>
              <li class="details-list">
                <span class="strength">${v.Strength}</span>
              </li>
              <li>
                <span><a href="javascript:void(0)" class="addmed" >+</a></span>
              </li>
            </ul>
          </div>
        </div>
      </div>
      `
    );
  });
}

$("#target").jqpaginator({
  showButtons: true,
  showInput: true,
  showNumbers: true,
  numberMargin: 1,
  itemsPerPage: itemsPerPage,
  data: drugs,
  render: renderer,
});

countries.forEach((element) => {
  $("#country-drop ul").append(
    `<li data-code="${element.code}" data-name="${element.name}">
      <img src="flags/${element.code.toLowerCase()}.png" class="flagstrap-icon" style="width:24px"/> ${
      element.name
    }
    </li>`
  );
});

let arr = [];
let coun = [];
let optim = [];
let limit = [];
let selectedno = "";
// reset function
function reset() {
  $("#country").text("country");
  $("#optimal").text("optimal formulary regimems");
  $("#limited").text("limited use list");
  arr = [];
  coun = [];
  optim = [];
  limit = [];
  optimarr = "";
  limitarr = "";
  $(".optimselectedno").text("");
  $(".limitselectedno").text("");
  $(".optimselectedno").removeClass("show");
  $(".limitselectedno").removeClass("show");
}

function countryDropdown(seletor) {
  // ------------------------------------------//

  let Selected = $(seletor);
  let Drop = $(seletor + "-drop");
  let DropItem = Drop.find("li");

  Selected.click(function () {
    Selected.toggleClass("open");
    Drop.toggle();
  });

  Drop.delegate("li", "click", function () {
    Selected.removeClass("open");
    Drop.hide();

    let item = $(this);
    if (seletor == "#country") {
      coun.push(item);
    }
    if (seletor == "#optimal") {
      optim.push(item);
      if (optim.length > 0) {
        $(".optimselectedno").addClass("show");
        $(".optimselectedno").text(`${optim.length}`);
      }
    }

    arr.push(item);
    console.log(arr, coun, optim, limit);
    Selected.html(item.html());
  });
}

countryDropdown("#country");
let medarr;
// selecing drugs
medarr = [
  {
    country: [],
    medicine_list: [],
  },
];

$("body").delegate(".addmed", "click", function () {
  // console.log("click");
  let detailsLists = $(this).parents(".details-lists");
  let name = detailsLists.find(".generic_name").text();
  let form = detailsLists.find(".form").text();
  let strength = detailsLists.find(".strength").text();

  let med_list = {
    generic_name: name,
    form: form,
    strength: strength,
  };

  const c = {
    name: coun[0][0].dataset.name,
    code: coun[0][0].dataset.code,
  };

  medarr[0].medicine_list.push(med_list);
  medarr[0].country.push(c);

  let htmltemplate = `

        <div class="product-card" id="productCard">
      
        <div class="product-details">
        <div class="d-flex justify-content-between mb-2">
        
          <span class="details-heading font-weight-bold text-uppercase d-block">Medicine</span>
          
          <span><a href="javascript:void(0)" class="btn btn-sm btn-danger deletemed ">Delete</a></span>
          
          </div>
          <ul class="details-listss d-flex flex-wrap justify-content-between list-unstyled">
            <li class="details-list">
              <span>Country</span>
              <span class=" countryoutput">
              <img src="https://www.countryflags.io/${c.code.toLowerCase()}/flat/64.png" class="flagstrap-icon" style="width:24px"/> ${
    c.name
  }
              </span>
            </li>
            <li class="details-list">
              <span>Generic Name</span>
              <span class="font-weight-bold">${name}</span>
            </li>
            <li class="details-list">
              <span>Form</span>
              <span class="font-weight-bold">${form}</span>
            </li>
             <li class="details-list">
              <span>Strength</span>
              <span class="font-weight-bold">${strength}</span>
            </li>
           
          </ul>
        </div>

      </div>
        `;
  $(".output").append(htmltemplate);

  if ($(".output").children().length > 1) {
    $(".output").show();
    $(".createheader").hide();
    $(".countryWrapList").hide();
  } else {
    $(".output").hide();
    $(".createheader").show();
    $(".countryWrapList").show();
  }
  // reset();

  $(".product-card").delegate(".deletemed", "click", function () {
    $(this).parents(".product-card").remove();
    if ($(".output").children().length > 1) {
      $(".output").show();
      $(".createheader").hide();
      $(".countryWrapList").hide();
    } else {
      $(".output").hide();
      $(".createheader").show();
      $(".countryWrapList").show();
    }
  });
});

if ($(".output").children().length > 1) {
  $(".output").show();
  $(".createheader").hide();
  $(".countryWrapList").hide();
} else {
  $(".output").hide();
  $(".createheader").show();
  $(".countryWrapList").show();
}

// share ui

$(".sharemed").on("click", function () {
  let countryarr = medarr[0].country[0];
  let medicinearr = medarr[0].medicine_list;
  console.log("share", countryarr, medicinearr);

  let countrytemp = ` <span class="countryoutput font-weight-bold">
              <img src="flags/${countryarr.code.toLowerCase()}.png" class="flagstrap-icon" style="width:30px"/>
              ${countryarr.name}
            </span>`;
  let medicinetemp = `
  `;

  medicinearr.forEach((element) => {
    medicinetemp += `
       <div class="col-md-12">
        <div class="product-card" id="productCard">
          <div class="product-details ">
            <ul class="details-lists d-flex mb-0 justify-content-between list-unstyled">
            
              <li class="details-list">
                <span class="generic_name">${element.generic_name}</span>
              </li>
              <li class="details-list">
                <span class="form">${element.form}</span>
              </li>
              <li class="details-list">
                <span class="strength">${element.strength}</span>
              </li>
              
            </ul>
          </div>
        </div>
      </div>
      `;
  });

  let temp = `
     <div class="product-card medUiCard">
        <div class="product-detailss p-2 border rounded bg-white">
          <div class="">
            ${countrytemp}
          </div>
          <span
            class="details-heading  d-block mt-1 "
            >Available Antiretroviral Medications</span
          >
          <div class="medicine-lists medUiCard-lists">
            <div class="col-md-12">
              <div class="list-header d-flex  pb-1">
                <div>Generic Name</div>
                <div>Form</div>
                <div>Strength</div>
                <div></div>
              </div>
            </div>
            ${medicinetemp}
          </div>
          <div class="trademark">Powered By CloudScript<sup>TM</sup> </div>
        </div>
      </div>
  `;

  $(".uioutput").html(temp);

  // domtoimage.toBlob(document.querySelector(".uioutput")).then(function (blob) {
  //   var img = new Image();
  //   img.crossOrigin = "anonymous";
  //   window.saveAs(blob, "Medform.png");
  //   // location.reload();
  // });
  domtoimage
    .toJpeg(document.querySelector(".uioutput"), { quality: 0.95 })
    .then(function (dataUrl) {
      var link = document.createElement("a");
      link.download = "Medicine_list.jpeg";
      link.href = dataUrl;
      link.click();
      location.reload();
    });
});

